//module.exports=(app1)=>
//{
    
    const notes =require('../controllers/note.controller.js');

    //Create a new Note
    app1.post('/notes', notes.create);

    //Retrieve all notes
    app1.get('/notes', notes.findAll);

    //Retrieve a single Note with noteId
    app1.get('notes/:noteId', notes.findOne);

    //Update a Note with noteId
    app1.put('notes/:noteId', notes.update);

    //Delete a Note with noteId
    app1.delete('notes/:noteId', notes.delete);

//}