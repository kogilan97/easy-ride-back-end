const express= require('express');
const journeys =require('../controllers/journeys_controller.js');
const verifytoken = require ('../api/helper/jwtauth.js');
const router = express.Router();

router.post('/newjourney', journeys.createJourney);
router.get('/viewjourneys/:journeyStatus', verifytoken.checkToken, journeys.viewJourneyDetails);
router.get('/viewsinglejourney/:journeyId',verifytoken.checkToken, journeys.viewSingleJourney);
router.get('/driver/:journeyStatus',verifytoken.checkToken, journeys.viewDriverJourneyDetails);
router.get('/driverOne/:journeyId',verifytoken.checkToken, journeys.viewDriverSingleJourney);
router.get('/alljourneys',journeys.allJournys);
router.delete('/deleteJourney/:journeyId',journeys.deleteJourney);


router.post('/driverAccept/:journeyId',verifytoken.checkToken, journeys.driverAccept);
router.post('/driverReject/:journeyId',verifytoken.checkToken, journeys.driverReject);
router.post('/driverStart/:journeyId',verifytoken.checkToken, journeys.driverStart);
router.post('/driverComplete/:journeyId',verifytoken.checkToken, journeys.driverComplete);
router.post('/passengerCancel/:journeyId',verifytoken.checkToken, journeys.passengerCancel);

module.exports = router;


    
