const express = require('express');
const users = require('../controllers/users_controller.js');
const verifytoken = require('../api/helper/jwtauth.js');
const verification = require('../controllers/verifications_controller.js');

const router = express.Router();

//Create a new User
router.post('/register', users.createUser);
//View all users
router.get('/', users.viewUsers);
//Login a user
router.post('/login', users.LoginUser);
//Delete a user
router.delete('/:userID', users.deleteUser);
//get profile details
router.post('/getprofiledetails', users.getprofiledetails);
// Verify an email address
router.post('/emailVerification', verification.emailVerification);
router.get('/verifiedEmail', verification.verifiedEmail);

//Verify Phone Numbers
router.post('/makePhoneRequest/:phoneNumber',verifytoken.checkToken, verification.makePhoneRequest);
router.post('/validatePhoneRequest',verifytoken.checkToken, verification.validatePhoneRequest);
router.post('/cancelPhoneRequest/:requestId', verifytoken.checkToken, verification.cancelPhoneRequest);

//Verify IC and License
router.post('/verifiedIc/:userId', verifytoken.checkToken, verification.verifiedIC);
router.post('/verifiedLicense/:userId', verifytoken.checkToken,verification.verifiedLicense);

//send feedback
router.post('/sendfeedback', verifytoken.checkToken,users.sendFeedback);
router.get('/feedback',users.viewFeedback);

module.exports = router;