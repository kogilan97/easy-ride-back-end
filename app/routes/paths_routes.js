const express = require('express');
const paths =require('../controllers/paths_controller.js');
const verifytoken = require ('../api/helper/jwtauth.js');

const router=express.Router();

router.post('/create', paths.createPath);
router.get('/view/:driverId', paths.viewPath);
router.post('/passengerview', paths.viewfilteredpath );
router.get('/viewAllWayPoints', paths.viewAllWayPoints);
router.get('/viewSearchOne/:pathId', paths.searchSinglePath);
router.post('/search', paths.searchPath);
module.exports=router;
