
 
const express = require('express');
const cars =require('../controllers/cars_controller.js');
const verifytoken = require ('../api/helper/jwtauth.js');
const multer= require('multer');


const router = express.Router();

//View All Cars
    router.get('/viewindividualcars', cars.viewindividualcars);
    
//Create and Upload all the cars
router.post('/addnewcar', cars.createcar);

//View all car details
router.get('/viewallcars/:userId', cars.viewallcars);

//Delete a car
router.delete('/deletecar/:carId', cars.deletecar);

//Upload a car image
router.put('/addcarimage/:carId',multer({ dest: 'temp/', limits: { fieldSize: 8 * 1024 * 1024 } }).single(
    'image_file' ), cars.updateById);
    
    module.exports = router;