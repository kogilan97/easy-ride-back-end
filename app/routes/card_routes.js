const express = require('express');
const credits = require('../controllers/credit_controller.js')
const verifytoken = require ('../api/helper/jwtauth.js');


const router=express.Router();

//card configurations
router.get('/getAll/:user_id', verifytoken.checkToken, credits.getAll);
router.delete('/delete/:user_id', verifytoken.checkToken, credits.deleteById);
router.post('/create', verifytoken.checkToken, credits.create);
router.post('/calculate', verifytoken.checkToken, credits.calculate);
router.post('/pay', verifytoken.checkToken, credits.makePayment);
router.get('/earnings/:userId', verifytoken.checkToken, credits.viewearning);
router.get('/commission',  credits.viewcommission);

//payment configurations
 

module.exports=router;
