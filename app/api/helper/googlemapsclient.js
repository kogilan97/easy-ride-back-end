const config = require('../../../config/token.js');



const googleMapsClient = require('@google/maps').createClient({
    key: config.googleapikey,
    Promise: Promise
  })

  module.exports = googleMapsClient;