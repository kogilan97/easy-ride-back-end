const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const feedbackSchema = mongoose.Schema({
    userId: String,
    email: String,
    comment: String

    //timestamps is used to add two new fields which are createdAt and updatedAt to the schema
}, {
    timestamps: true,
    versionKey: false // You should be aware of the outcome after set to false
}, )

module.exports = mongoose.model('feedback', feedbackSchema);