const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const journeySchema = mongoose.Schema({
    dateTime: String,
    carId: String,
    driverResponse: String,
    userId: String,
    driverId: String,
    journeyStatus: String,
    startPointLat: String,
    startPointLong: String,
    endPointLat: String,
    endPointLong: String, 
    price: String
    //timestamps is used to add two new fields which are createdAt and updatedAt to the schema
}, {
    timestamps: true,
    versionKey: false // You should be aware of the outcome after set to false
}, )

module.exports = mongoose.model('Journeys', journeySchema);