const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const pathSchema = mongoose.Schema ({
dateTime:String,
driverId: String,
pathName: String,
carId: String,

//timestamps is used to add two new fields which are createdAt and updatedAt to the schema
},
{
    timestamps:true,
    versionKey: false // You should be aware of the outcome after set to false
},
)

module.exports=mongoose.model('Paths', pathSchema);

