const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const CarSchema = mongoose.Schema ({
userid: String,
emailaddress:String,
carnumber: String,
carmodel: String,
capacity: String,
image_file:String
//timestamps is used to add two new fields which are createdAt and updatedAt to the schema
},
{
    timestamps:true,
    versionKey: false // You should be aware of the outcome after set to false
},
)

module.exports=mongoose.model('Cars', CarSchema);

