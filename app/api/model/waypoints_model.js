const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

const wayPointsSchema = mongoose.Schema ({
pathId: String, 
latitude: String,
longitude: String,
isStart: Boolean,
isEnd: Boolean
},
{
    timestamps:true,
    versionKey: false // You should be aware of the outcome after set to false
},
)

module.exports=mongoose.model('wayPoints', wayPointsSchema);

