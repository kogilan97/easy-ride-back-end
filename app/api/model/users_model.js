const mongoose = require('mongoose');               

const UserSchema = mongoose.Schema ({
email: {
    type:String,
    unique:true
},
username: String,
password: String,
phone: String,
emailverification: Boolean,
phoneverification: Boolean,
icverification: Boolean,
licenseverification: Boolean,
approle: String
},
{
    timestamps:true,
    versionKey: false 
},
)
module.exports=mongoose.model('Users', UserSchema);

