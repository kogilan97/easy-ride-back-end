const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const paymentSchema = mongoose.Schema ({
journeyId:String,
dateTime: String,
amount: String,
userId: String,

//timestamps is used to add two new fields which are createdAt and updatedAt to the schema
},
{
    timestamps:true,
    versionKey: false // You should be aware of the outcome after set to false
},
)

module.exports=mongoose.model('payments', paymentSchema);

