const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema ({
title: String,
content:String
//timestamps is used to add two new fields which are createdAt and updatedAt to the schema
},
{
    timestamps:true
});

module.exports=mongoose.model('Note', NoteSchema);

