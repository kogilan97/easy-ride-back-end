const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const creditSchema = mongoose.Schema ({
    user_id: String,
    brand:String,
    country: String,
    exp_month: Number,
    exp_year: Number,
    fingerprint: String,
    token: String,
    last4: String,
   
}, {
    timestamps:true,
    versionKey: false 
    }
)
module.exports=mongoose.model('creditcard', creditSchema);

    