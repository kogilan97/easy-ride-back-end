const journeys = require('../api/model/journeys_model.js');
const users = require('../api/model/users_model.js');
const cars = require('../api/model/cars_model.js');
const payments = require('../api/model/payment_model.js');

module.exports = {

    viewJourneyDetails: function (req, res, next) {

        let journeyDetailsArray = [];
        journeys.find({
            userId: req.headers.userid,
            journeyStatus: req.params.journeyStatus
        }, function (err, journeyInfo) {
            if (err) {
                next(err);
            } else {
                if (journeyInfo.length <= 0) {
                    return res.status(200).send({
                        status: true,
                        message: 'No Journey Details',
                    });
                } else {

                    var counter = 1;
                    for (let product of journeyInfo) {

                        cars.findOne({
                            _id: product.carId

                        }, function (err, carInfo) {

                            if (err) {
                                next(err)
                            } else {
                                console.log(carInfo);
                                journeyDetailsArray.push({
                                    _id: product._id.toString(),
                                    carNumber: carInfo.carnumber,
                                    carModel: carInfo.carmodel,
                                    driverResponse: product.driverResponse,
                                    dateTime: product.dateTime,
                                    image_file: carInfo.image_file

                                })
                                console.log("counter:" + counter);
                                console.log(journeyInfo.length);

                                if (counter == journeyInfo.length) {

                                    return res.status(200).send({
                                        status: true,
                                        message: 'Journey Records have been Retrieved',
                                        journeyInfo: journeyDetailsArray
                                    });
                                }
                                counter++;
                            }

                        })

                    }
                }
            }
        })
    },
    viewSingleJourney: function (req, res, next) {
        journeys.findOne({
            _id: req.params.journeyId
        }, function (err, journeyInfo) {
            if (err) {
                next(err);
            } else {
                if (journeyInfo == null) {
                    return res.status(200).send({
                        status: true,
                        message: 'No journey details',
                    });
                } else {
                    users.findOne({
                        _id: journeyInfo.driverId
                    }, function (err, driverInfo) {
                        if (err) {
                            next(err);
                        } else {
                            if (driverInfo == null) {
                                return res.status(200).send({
                                    status: true,
                                    message: 'Driver details are not found',
                                });
                            } else {

                                cars.findOne({
                                        _id: journeyInfo.carId
                                    }, function (err, carInfo) {
                                        if (err) {
                                            next(err);
                                        } else {
                                            if (carInfo == null) {
                                                return res.status(200).send({
                                                    status: true,
                                                    message: 'Car details are not found',
                                                });
                                            } else {
                                                var info = journeyInfo.toJSON();
                                                info['driverInfo'] = driverInfo;
                                                info['carInfo'] = carInfo

                                                return res.status(200).send({
                                                    status: true,
                                                    message: 'Journey Records has been retrieved',
                                                    journey: info

                                                });


                                            }
                                        }
                                    }

                                )
                            }
                        }
                    });
                }
            }
        })
    },
    createJourney: function (req, res, next) {

        var totalDistance = distance1(req.body.startPointLat, req.body.startPointLong, req.body.endPointLat, req.body.endPointLong);
        var cost = ((totalDistance / 1000) * 1).toFixed(2);

        function distance1(lat1, lon1, lat2, lon2) {
            var R = 6371; // km (change this constant to get miles)
            var dLat = (lat2 - lat1) * Math.PI / 180;
            var dLon = (lon2 - lon1) * Math.PI / 180;
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return Math.round(d * 1000);
        }

        journeys.create({
            dateTime: req.body.dateTime,
            carId: req.body.carId,
            driverResponse: "Pending",
            userId: req.body.userId,
            driverId: req.body.driverId,
            journeyStatus: "Upcoming",
            startPointLat: req.body.startPointLat,
            startPointLong: req.body.startPointLong,
            endPointLat: req.body.endPointLat,
            endPointLong: req.body.endPointLong,
            price: cost

        }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({
                    status: true,
                    message: "Journey has Successfully Created",
                    journey: result,
                });
        })
    },
    viewDriverJourneyDetails: function (req, res, next) {
        let journeyDetailsArray = [];
        journeys.find({
            driverId: req.headers.userid,
            journeyStatus: req.params.journeyStatus
        }, function (err, journeyInfo) {
            if (err) {
                next(err);
            } else {
                if (journeyInfo.length <= 0) {
                    return res.status(200).send({
                        status: true,
                        message: 'No Journey Details',
                    });
                } else {

                    var counter = 1;
                    for (let product of journeyInfo) {

                        cars.findOne({
                            _id: product.carId

                        }, function (err, carInfo) {

                            if (err) {
                                next(err)
                            } else {
                                users.findOne({
                                    _id: product.userId
                                }, function (err, userInfo) {
                                    if (err) {
                                        next(err)
                                    } else {
                                        console.log(carInfo);
                                        journeyDetailsArray.push({
                                            _id: product._id.toString(),
                                            carNumber: carInfo.carnumber,
                                            carModel: carInfo.carmodel,
                                            driverResponse: product.driverResponse,
                                            dateTime: product.dateTime,
                                            image_file: carInfo.image_file,
                                            passengerId: userInfo._id,
                                            passsengerName: userInfo.username,
                                            passengerPhone: userInfo.phone

                                        })
                                        console.log("counter:" + counter);
                                        console.log(journeyInfo.length);

                                        if (counter == journeyInfo.length) {

                                            return res.status(200).send({
                                                status: true,
                                                message: 'Journey Records have been Retrieved',
                                                journeyInfo: journeyDetailsArray
                                            });
                                        }
                                        counter++;
                                    }
                                })
                            }
                        })

                    }
                }
            }
        })
    },
    viewDriverSingleJourney: function (req, res, next) {
        journeys.findOne({
            _id: req.params.journeyId
        }, function (err, journeyInfo) {
            if (err) {
                next(err);
            } else {
                if (journeyInfo == null) {
                    return res.status(200).send({
                        status: true,
                        message: 'No journey details',
                    });
                } else {
                    users.findOne({
                        _id: journeyInfo.userId
                    }, function (err, driverInfo) {
                        if (err) {
                            next(err);
                        } else {
                            if (driverInfo == null) {
                                return res.status(200).send({
                                    status: true,
                                    message: 'Driver details are not found',
                                });
                            } else {

                                cars.findOne({
                                    _id: journeyInfo.carId
                                }, function (err, carInfo) {
                                    if (err) {
                                        next(err);
                                    } else {
                                        if (carInfo == null) {
                                            return res.status(200).send({
                                                status: true,
                                                message: 'Car details are not found',
                                            });
                                        } else {
                                            var totalAmount = journeyInfo.price;
                                            var commission = (totalAmount * (0.15)).toFixed(2);
                                            var earned = (totalAmount - commission).toFixed(2);

                                            var payment = {
                                                totalAmount: totalAmount,
                                                commission: commission,
                                                earned: earned
                                            }

                                            var info = journeyInfo.toJSON();
                                            info['userInfo'] = driverInfo;
                                            info['carInfo'] = carInfo;
                                            info['paymentInfo'] = payment;

                                            return res.status(200).send({
                                                status: true,
                                                message: 'Journey Records has been retrieved',
                                                journey: info

                                            });
                                        }
                                    }
                                })
                            }
                        }
                    });
                }
            }
        })
    },

    driverAccept: function (req, res, next) {

        journeys.updateOne({
                _id: req.params.journeyId
            }, {
                $set: {
                    driverResponse: "Accepted",
                }
            },
            function (err, journeyInfo) {
                if (err) {
                    next(err);
                } else {
                    res.json({
                        status: true,
                        message: "The journey has been accepted",
                    })
                }
            }
        )


    },

    driverReject: function (req, res, next) {

        journeys.updateOne({
                _id: req.params.journeyId
            }, {
                $set: {
                    driverResponse: "Rejected",
                    journeyStatus: "Past"
                }
            },
            function (err, journeyInfo) {
                if (err) {
                    next(err);
                } else {
                    res.json({
                        status: true,
                        message: "The journey has been rejected",
                    })
                }
            }
        )
    },
    driverStart: function (req, res, next) {
        journeys.updateOne({
                _id: req.params.journeyId
            }, {
                $set: {
                    driverResponse: "Started",
                    journeyStatus: "Current"
                }
            },
            function (err, journeyInfo) {
                if (err) {
                    next(err);
                } else {
                    res.json({
                        status: true,
                        message: "The journey has started",
                    })
                }
            }
        )
    },
    driverComplete: function (req, res, next) {

        payments.findOne({
                journeyId: req.params.journeyId
            }, function (err, paymentInfo) {

                if (paymentInfo==null) {
                    res.json({
                        status: false,
                        message: "The payment has not been made for the journey",
                    })
                }
                else
                {
                    if(paymentInfo)
                    {
                        journeys.updateOne({
                            _id: req.params.journeyId
                        }, {
                            $set: {
                                driverResponse: "Completed",
                                journeyStatus: "Past"
                            }
                        },
                        function (err, journeyInfo) {
                            if (err) {
                                next(err);
                            } else {
                                res.json({
                                    status: true,
                                    message: "The payment has been made for the journey",
                                })
                            }
                        }
                    )
                    }
                }
            }

        )

    },
    passengerCancel: function (req, res, next) {

        journeys.updateOne({
                _id: req.params.journeyId
            }, {
                $set: {
                    driverResponse: "Cancelled",
                    journeyStatus: "Past"
                }
            },
            function (err, journeyInfo) {
                if (err) {
                    next(err);
                } else {
                    res.json({
                        status: true,
                        message: "The journey has been cancelled",
                    })
                }
            }
        )
    },

    allJournys: function(req,res,next){
        journeys.find()
        .then(journeyDetails => {
            res.json({
                status: true,
                message: "All the journey details are retrieved",
                product: journeyDetails

            });
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving journey details."
            });
        });
    },

    deleteJourney: function (req, res, next) {

        journeys.findOneAndDelete(
            {
                _id:req.params.journeyId
            })
            .then(deletejourney => {
                if (!deletejourney) {
                    return res.status(404).send({
                        message: "Journey not found with id " + req.params.journeyId
                    })
                } else {
                    res.send({
                        message: "Journey deleted successfully!"
                    });
                }

            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "Journey not found with id " + req.params.journeyId
                    });
                }
                return res.status(500).send({
                    message: "Could not delete the journey with id " + req.params.journeyId
                });
            });
    }
}