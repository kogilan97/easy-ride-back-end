const users = require('../api/model/users_model.js');
const tokenconfig = require('../../config/token.js');
const jwtauth = require('../api/helper/jwtauth.js');
const feedback =require('../api/model/feedback_model.js')
const jwt = require('jsonwebtoken');


const bcrypt = require('bcryptjs');


//Create and Save a new User

module.exports = {

    getprofiledetails: function (req, res, next) {
        users.findOne({
            _id: req.body.userId
        }, function (err, userInfo) {
            if (err) {
                next(err);
            } else {

                return res.status(200).send({
                    status: true,
                    message: 'Profile Record has been Retrieved',
                    userInfo
                });

            }


        })
    },


    createUser: function (req, res, next) {

        users.findOne({
            email: req.body.emailaddress
        }, function (err, userInfo) {

            if (err) {
                next(err);
            } else {
                if (userInfo) {
                    return res.send({
                        status: false,
                        message: 'Email Address Already exists'
                    });
                } else {
                    bcrypt.hash(req.body.password, 10, function (err, hash) {
                        console.log(hash);
                        if (err) {
                            next(err);
                        } else {
                            users.create({
                                username: req.body.username,
                                password: hash,
                                email: req.body.emailaddress,
                                phone: req.body.phonenumber,
                                emailverification: false,
                                phoneverification: false,
                                icverification: false,
                                licenseverification: false,
                                approle: "Passenger"
                            }, function (err, result) {
                                if (err)
                                    next(err);
                                else

                                    res.json({
                                        status: true,
                                        message: "Successfully Registered",
                                        product: result,

                                    });
                            })
                        }
                    })

                }
            }
        })
    },

    //Retrieve all users from the database
    viewUsers: function (req, res, next) {
        users.find()
            .then(userdetails => {
                res.json({
                    status: true,
                    message: "All the user details are retrieved",
                    product: userdetails

                });
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving users."
                });
            });
    },

    // Delete a user with an User id

    deleteUser: function (req, res, next) {

        users.findOneAndDelete(
            {
                email:req.params.userID
            })
            .then(deleteuser => {
                if (!deleteuser) {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userID
                    })
                } else {
                    res.send({
                        message: "User deleted successfully!"
                    });
                }

            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userID
                    });
                }
                return res.status(500).send({
                    message: "Could not delete the user with id " + req.params.userID
                });
            });
    },
 // Login a user 

    LoginUser: function (req, res, next) {

        users.findOne({
            email: req.body.emailaddress,

        }, function (err, userInfo) {

            if (err) {
                next(err);
            } else {
                if (userInfo == null) {
                    return res.status(200).send({
                        status: false,
                        message: 'Incorrect Email Address'
                    });
                } else {
                    bcrypt.compare(req.body.password, userInfo.password, function (err, result) {
                        if (result == true) {
                            const token = jwt.sign({
                                    email: userInfo.emailaddress
                                },
                                tokenconfig.secret, {
                                    expiresIn: 86400 // expires in 24 hours
                                }
                            );

                            return res.status(200).send({
                                status: true,
                                message: 'You have successfully logged into the system',
                                token: token,
                                userInfo
                            });
                        } else {
                            return res.status(200).send({
                                status: false,
                                message: 'Incorrect Password'
                            });
                        }
                    })

                }
            }
        })
    },

    sendFeedback: function (req, res, next) {

        feedback.create({

            userId:req.body.userId,
            email:req.body.email,
            comment:req.body.comment

        }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({
                    status: true,
                    message: "Feedback has been sent to the administrator"
                });
        })
    },
    // Search details of a single user
    viewFeedback:function(req,res,next) {

        feedback.find()
        .then(feedbackDetails => {
            res.json({
                status: true,
                message: "All the feedback details are retrieved",
                product: feedbackDetails

            });
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving users."
            });
        });
    }

    //create a new function from here onwards
    //for accessing mongoDB methods, just write in the model class name and find for findByID, create, find, findByIDRemove and etc..
}