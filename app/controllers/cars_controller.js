const cars = require('../api/model/cars_model.js');
const tokenconfig = require('../../config/token.js');
const jwtauth = require('../api/helper/jwtauth.js');
const jwt = require('jsonwebtoken');
const aws = require('aws-sdk');
const fs = require('fs');

module.exports = {

    viewallcars: function (req, res, next) {

        cars.find({
            userid: req.params.userId
            })
            .then(cardetails => {
                res.json({
                    status: true,
                    message: "All the car details are retrieved",
                    cardetails: cardetails

                });
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving car details."
                });
            });


    },

    deletecar: function (req, res, next) {

        cars.findByIdAndRemove(req.params.carId)
            .then(cars => {
                if (!cars) {
                    return res.send({
                        message: "Car not found with id " + req.params.carId,
                        status: false
                    });
                } else {
                    res.send({
                        message: "Car deleted successfully!",
                        status: true
                    });
                }
            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.send({
                        message: "Car not found with id " + req.params.carId,
                        status: false
                    });
                }
                return res.send({
                    message: "Could not delete the car with id " + req.params.carId,
                    status: false
                });
            });
    },

    viewindividualcars: function (req, res, next) {


    },

    createcar: function (req, res, next) {

        cars.create({
            userid: req.body.userid,
            emailaddress: req.body.email,
            carnumber: req.body.carnumber,
            carmodel: req.body.carmodel,
            capacity: req.body.capacity

        }, function (err, result) {
            if (err)
                next(err);
            else

                res.json({
                    status: true,
                    message: "Car has been successfully added",
                    product: result,

                });
        })
    },

    updateById: function (req, res, next) {

        aws.config.setPromisesDependency();
        aws.config.update({
            accessKeyId: "AKIAVTBIYANF56RFPM6M",
            secretAccessKey: "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",
            region: "us-east-1"
        });
        const s3 = new aws.S3();
        var params = {
            ACL: 'public-read',
            Bucket: "car-images-easyride",
            Body: fs.createReadStream(req.file.path),
            Key: `uploadImage/${req.file.originalname}`
        };

        s3.upload(params, (err, data) => {
            if (err) {
                console.log('Error occured while trying to upload to S3 bucket', err);
            }
            if (data) {
                fs.unlinkSync(req.file.path); // Empty temp folder
                const locationUrl = data.Location;

                cars.updateOne({
                    _id: req.params.carId
                }, {
                    $set: {
                        image_file: locationUrl
                    }
                }, function (err, carInfo) {
                    if (err)
                        next(err);
                    else {
                        res.json({
                            status: true,
                            message: "Image Uploaded successfully",
                            carInfo: carInfo
                        });
                    }
                });

            }
        });



    },

}