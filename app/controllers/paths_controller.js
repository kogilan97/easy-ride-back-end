const paths = require('../api/model/paths_model.js');
const waypoints = require('../api/model/waypoints_model.js');
const googleMapsClient = require('../api/helper/googlemapsclient.js');
const users = require('../api/model/users_model.js');
const cars = require('../api/model/cars_model.js');
const moment = require('moment');


module.exports = {
    createPath: function (req, res, next) {

        let wayPointsArray = [];
        paths.create({
            dateTime: req.body.dateTime,
            driverId: req.body.driverId,
            pathName: req.body.pathName,
            carId: req.body.carId
        }, function (err, result) {
            if (err)
                next(err);
            else
                for (let product of req.body.wayPoints) {

                    wayPointsArray.push({
                        pathId: result._id.toString(),
                        latitude: product.latitude,
                        longitude: product.longitude,
                        isStart: product.isStart,
                        isEnd: product.isEnd
                    })
                }
            waypoints.collection.insertMany(wayPointsArray, function (err, results) {
                if (err)
                    next(err);
                else
                    res.json({
                        status: true,
                        message: "Path has been successfully Created",
                        result: results.ops,
                    });
            })
        })
    },
    viewPath: function (req, res, next) {
        paths.find({
                driverId: req.params.driverId
            },
            function (err, pathInfo) {
                if (err) {
                    next(err);
                } else {
                    return res.status(200).send({
                        status: true,
                        message: 'Path Records have been Retrieved',
                        pathInfo
                    });
                }
            }
        )
    },

    searchSinglePath: function (req, res, next) {

        let retrievedPathInfo = [];
        var driverId1;
        var carId1;

        paths.findOne({
                _id: req.params.pathId
            },
            function (err, pathInfo) {

                if (err) {
                    next(err);
                } else {                         
                   users.findOne({
                            _id: pathInfo.driverId
                        },
                        function (err, driverInfo) {

                            cars.findOne({
                                _id: pathInfo.carId
                            },
                            function (err, carInfo) {

                                if (err) {
                                    next(err);
                                } else {
                                    return res.status(200).send({
                                        status: true,
                                        message: 'Path Records have been Retrieved',
                                        path:{
                                        dateTime: pathInfo.dateTime,
                                        pathName: pathInfo.pathName,
                                        driverName: driverInfo.username,
                                        driverId: driverInfo._id,
                                        carId:carInfo._id,
                                        email: driverInfo.email,
                                        phoneNumber: driverInfo.phone,
                                        carModel: carInfo.carmodel,
                                        carCapacity: carInfo.capacity,
                                        carNumber: carInfo.carnumber  
                                        }  
                                    });
        
                               }

                            })

                        })

                }
            }
        )
    },

    viewfilteredpath: function (req, res, next) {

        googleMapsClient.elevation({
            locations: {
                lat: 40.7118,
                lng: -111.9679
            }
        }, (err, response) => {
            console.log("Entering layer 1")
            if (!err && response.status === 200) {
                console.log("Entering layer 2")
                return res.send({
                    response
                })
            } else {
                console.log("Entering layer 3")
                return err;
            }
        })

    },

    viewAllWayPoints: function (req, res, next) {

        waypoints.find({},
            function (err, pathInfo) {

                return res.status(200).send({
                    status: true,
                    message: 'Waypoint records have been Retrieved',
                    pathInfo
                });

            })

    },

    searchPath: function (req, res, next) {

        let wayPointsArray2 = [];
        let wayPointsArray3 = [];
        let allDateTime = [];
        var startTime = moment(req.body.dateTime).subtract(1, 'hours').format();
        var endTime = moment(req.body.dateTime).add(1, 'hours').format();

        // console.log(startTime);
        // console.log(endTime);  

        paths.find({},
            function (err, pathInfo) {
                if (err) {
                    next(err);
                } else {

                    for (let product of pathInfo) {
                        const check = moment(product.dateTime).isBetween(startTime, endTime);
                        // console.log(product._id);
                        if (check == true) {
                            allDateTime.push({
                                dateTime: product.dateTime,
                                pathId: product._id,
                                pathName: product.pathName,
                                driverId: product.driverId
                            })
                        }
                    }


                    var counter = 0
                    for (let wayPoint of allDateTime) {

                        var number = 1;
                        waypoints.find({
                                pathId: wayPoint.pathId
                            },
                            function (err, wayPointsInfo) {
                                if (err) {
                                    console.log(err)
                                    // next(err);
                                } else {
                                    counter = counter + 1;
                                    var startflag = 0;
                                    var endflag = 0;
                                    //  console.log(wayPointsInfo);
                                    for (let wayPoint2 of wayPointsInfo) {

                                        var storedpoints = {
                                            lat: wayPoint2.latitude,
                                            lng: wayPoint2.longitude
                                        };
                                        var userstartingpoint = {
                                            lat: req.body.startLatitude,
                                            lng: req.body.startLongitude
                                        };

                                        var userendingpoint = {
                                            lat: req.body.endLatitude,
                                            lng: req.body.endLongitude
                                        };

                                        var isStartingPointNear = arePointsNear(storedpoints, userstartingpoint, 5);
                                        var isEndingPointNear = arePointsNear(storedpoints, userendingpoint, 5);


                                        if (isStartingPointNear == true) {
                                            var tempWayPoint = wayPoint2;
                                            startflag = 1;
                                        }

                                        if (isEndingPointNear == true) {
                                            endflag = 1;

                                        }

                                        if (startflag == 1 && endflag == 1) {



                                            paths.find({
                                                    _id: tempWayPoint.pathId
                                                },
                                                function (err, wayPointsInfo2) {



                                                    callback(wayPointsInfo2);


                                                })

                                            wayPointsArray2.push(

                                                tempWayPoint

                                            )
                                            startflag = 0;
                                            endflag = 0;
                                        }

                                    }


                                    function callback(product) {

                                        console.log(product);
                                    }

                                    if (counter === allDateTime.length) {

                                        res.json({
                                            status: true,
                                            message: "All waypoints have been retrieved",
                                            paths: wayPointsArray2

                                        })
                                    }
                                }
                            }
                        )
                    }
                }

                function arePointsNear(checkPoint, centerPoint, km) {
                    var ky = 40000 / 360;
                    var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
                    var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
                    var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
                    return Math.sqrt(dx * dx + dy * dy) <= km;
                }
            });

    },

    getAll: function (req, res, next) {
        let productList = [];

        websiteModel.find({}, function (err, website) {
            if (err) {
                next(err);
            } else {
                var counter = 0

                for (let product of website) {

                    watchlistModel.findOne({
                        product_id: product._id,
                        user_id: req.query.user_id,
                    }, function (err, watchlist) {



                        if (watchlist != null) {
                            callback(product, true)

                        } else {
                            callback(product, false)
                        }
                    })

                }

                function callback(product, val) {
                    counter = counter + 1
                    if (val) {

                        productList.push({
                            _id: product._id,
                            title: product.title,
                            subtitle: product.subtitle,
                            current_price: product.current_price,
                            watchlist: true,
                            isFeatured: product.isFeatured,
                            image_file: product.image_file || null

                        });

                    } else {

                        productList.push({
                            _id: product._id,
                            title: product.title,
                            subtitle: product.subtitle,
                            current_price: product.current_price,
                            watchlist: false,
                            isFeatured: product.isFeatured,
                            image_file: product.image_file || null
                        });
                    }

                    if (website.length === counter) {
                        if (productList.length > 0) {
                            res.json({
                                status: true,
                                message: productList.length + " websites found",
                                product_list: {
                                    product: shuffleArray.shuffle(productList)
                                }
                            });
                        } else {
                            res.json({
                                status: true,
                                message: "No website found !"
                            })
                        }
                    }
                    //console.log(productList)

                }


            }
        });
        return productList;
    },
}