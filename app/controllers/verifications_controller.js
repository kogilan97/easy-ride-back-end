const users = require('../api/model/users_model.js');
const nodemailer = require('nodemailer');
const xoauth2 = require('xoauth2');
var AWS = require('aws-sdk');
const nexmo = require('../api/helper/nexmoauth.js');

AWS.config.update({
    region: 'us-east-1'
});



var senderEmail;
var rand, host, link;

module.exports = {

    emailVerification: function (req, res, next) {
        users.findOne({
            email: req.body.userId
        }, function (err, userInfo) {

            if (err) {
                next(err);
                return res.status(200).send({
                    status: true,
                    message: 'An error has occured',
                });
            } else {
                
                senderEmail = req.body.userId;
                rand = Math.floor((Math.random() * 100) + 54);
                host = req.get('host');
                console.log(host);
                link = "http://" + host + "/users/verifiedEmail?id=" + rand;


                var params = {
                    Destination: {
                        /* required */
                        CcAddresses: [

                        ],
                        ToAddresses: [
                            req.body.userId,
                            /* more items */
                        ]
                    },
                    Message: {
                        /* required */
                        Body: {
                            /* required */
                            Html: {
                                Charset: "UTF-8",
                                Data: "Hello,<br> Please Click on the link to verify your email.<br><a href=" + link + ">Click here to verify</a>"
                            },
                            Text: {
                                Charset: "UTF-8",
                                Data: "TEXT_FORMAT_BODY"
                            }
                        },
                        Subject: {
                            Charset: 'UTF-8',
                            Data: 'Email Verification from Easy Ride'
                        }
                    },
                    Source: 'kogilankrishnansamy@gmail.com'

                }


                // Create the promise and SES service object
                var sendPromise = new AWS.SES({
                    apiVersion: '2010-12-01'
                }).sendEmail(params).promise();
                sendPromise.then(
                    function (data) {
                        console.log(data.MessageId);

                        return res.status(200).send({
                            status: true,
                            message: 'A verification email has been sent to your email address',
                        });

                    }).catch(
                    function (err) {
                        console.error(err, err.stack);
                        return res.status(200).send({
                            status: true,
                            message: err,
                        });
                    });

            }

        })
    },
    verifiedEmail: function (req, res, next) {
        console.log(req.protocol + "://" + req.get('host'));
        if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
            console.log("Domain is matched. Information is from Authentic email");
            if (req.query.id == rand) {
                console.log("email is verified");
                res.end("<p>Email " + senderEmail + " has been Successfully verified </p>");


                users.updateOne({
                        email: senderEmail
                    }, {
                        $set: {
                            emailverification: true
                        }
                    },
                    function (err, userinfo) {
                        if (err) {
                            next(err);
                        } else {
                            console.log("Email verification has been done");
                            res.end("<p>Email verification has been successfully done</p>");
                        }

                    }
                )

            } else {
                console.log("email is not verified");
                res.end("<p>Bad Request</p>");
            }
        } else {
            res.end("<p>Request is from unknown source</p>");
        }
    },

    makePhoneRequest: function (req, res, next) {
        console.log(req.params.phoneNumber);
        nexmo.verify.request({
            number: req.params.phoneNumber,
            brand: "Nexmo",
            code_length: "4"
        }, (err, result) => {
            console.log(err ? res.json({
                    status: true,
                    result: err,
                }) :
                res.json({
                    status: true,
                    message: "A verification token has been sent to your phone",
                    result: result,
                })
            )
        });
    },


    validatePhoneRequest: function (req, res, next) {

        nexmo.verify.check({
            request_id: req.body.reqId,
            code: req.body.reqCode
        }, (err, result) => {
            console.log(err ? res.json({
                    status: false,
                    err: err,
                }) :

                (result.error_text ? res.json({
                    status: true,
                    result: result,
                }) :

                users.updateOne({
                    _id: req.body.userId
                }, {
                    $set: {
                        phoneverification: true
                    }
                },
                function (err, userinfo) {
                    if (err) {
                        next(err);
                    } else {
                        res.json({
                            status: true,
                            message: "Your phone number has been validated.",
                            result: result,
                        })
                    }

                }
            ))
            )
        });
    },

    cancelPhoneRequest: function (req, res, next) {

        nexmo.verify.control({
            request_id: req.params.requestId,
            cmd: 'cancel'
        }, (err, result) => {
            console.log(err ? res.json({
                    status: false,
                    err: err,
                }) :
                res.json({
                    status: true,

                    result: result,
                }))
        });

    },


    verifiedIC : function(req,res,err)
    {
        users.updateOne({
            _id: req.params.userId
        }, {
            $set: {
                icverification: true
            }
        },
        function (err, userinfo) {
            if (err) {
                next(err);
            } else {
                res.json({
                    status: true,
                    message: "Your identification card has been verified.",                
                })
            }

        }
    )
    },

    verifiedLicense : function(req,res,err)
    {
        users.updateOne({
            _id: req.params.userId
        }, {
            $set: {
                licenseverification: true
            }
        },
        function (err, userinfo) {
            if (err) {
                next(err);
            } else {
                res.json({
                    status: true,
                    message: "Your driving license has been verified.",                
                })
            }
        }
    )
    }


}