const stripe = require("stripe")("sk_test_BDTCBswTA5665XBI0loowkHw002jLe7YV0");
const users_modal = require('../api/model/users_model.js');
const creditCardModel = require('../api/model/credit_model.js');
const paymentModel = require('../api/model/payment_model.js');
const earningModel = require('../api/model/earning_model.js');
const journeys = require('../api/model/journeys_model.js');

module.exports = {

    getAll: function (req, res, next) {
        creditCardModel.findOne({
            user_id: req.params.user_id
        }, function (err, result) {
            if (err) {
                res.json({
                    status: false,
                    message: "No credit card found",

                });

            } else if (result == null) {
                res.json({
                    status: false,
                    message: "No credit card found",

                });

            } else {
                res.json({
                    status: true,
                    message: "Credit Card found",
                    card: result,
                });

            }
        })
    },

    deleteById: function (req, res, next) {
        creditCardModel.findOneAndRemove({
            user_id: req.params.user_id
        }, function (err, creditCard) {
            if (err)
                next(err);
            else {

                res.json({
                    status: true,
                    message: "Credit Card deleted successfully!!!",

                });
            }
        });
    },
    create: function (req, res, next) {
        creditCardModel.findOne({
                user_id: req.body.user_id
            }, function (err, userInfo) {
                if (err) {
                    next(err);

                } else {
                    if (userInfo) {
                        return res.status(400).send({
                            status: false,
                            message: 'Credit card already added.'
                        });
                    } else {

                        stripe.tokens.create({
                            card: {
                                number: req.body.card_number,
                                exp_month: req.body.exp_month,
                                exp_year: req.body.exp_year,
                                cvc: req.body.cvc
                            }
                        }, function (err, token) {
                            // asynchronously called
                            if (err) {
                                res.json({
                                    status: false,
                                    message: err.message
                                })
                            } else {
                                creditCardModel.create({
                                    user_id: req.body.user_id,
                                    brand: token.card.brand,
                                    country: token.card.country,
                                    exp_month: token.card.exp_month,
                                    exp_year: token.card.exp_year,
                                    fingerprint: token.card.fingerprint,
                                    token: token.id,
                                    last4: token.card.last4,

                                }, function (err, result) {
                                    if (err)
                                        next(err);
                                    else
                                        res.json({
                                            status: true,
                                            message: "Successfully added credit card",
                                            card: {
                                                user_id: result.user_id,
                                                brand: result.brand,
                                                country: result.country,
                                                exp_month: result.exp_month,
                                                exp_year: result.exp_year,
                                                fingerprint: result.fingerprint,
                                                token: result.token,
                                                last4: result.last4,
                                            },
                                            ori: token
                                        })
                                });


                            }
                        });

                    }
                }
            }

        )

    },
    calculate: function (req, res, next) {
        var totalDistance = distance1(req.body.startLat, req.body.startLong, req.body.endLat, req.body.endLong);
        var cost = ((totalDistance / 1000) * 1).toFixed(2);
        console.log(cost);

        res.json({
            status: true,
            message: "Cost has been calculated",
            totalCost: cost
        })

        function distance1(lat1, lon1, lat2, lon2) {
            var R = 6371; // km (change this constant to get miles)
            var dLat = (lat2 - lat1) * Math.PI / 180;
            var dLon = (lon2 - lon1) * Math.PI / 180;
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return Math.round(d * 1000);
        }

    },
    makePayment: function (req, res, next) {
        paymentModel.create({
            journeyId: req.body.journeyId,
            dateTime: req.body.dateTime,
            amount: req.body.amount,
            userId: req.body.userId
        }, function (err, result) {
            if (err)
                next(err);
            else
                journeys.updateOne({
                        _id: req.body.journeyId
                    }, {
                        $set: {
                            driverResponse: "Paid"
                        }
                    },
                    function (err, journeyInfo) {

                        if (err) {
                            next(err);
                        } else {
                            var totalAmount = req.body.amount;
                            var commission1 = (totalAmount * (0.15)).toFixed(2);
                            var earned = (totalAmount - commission1).toFixed(2);
                            earningModel.create({
                                journeyId: req.body.journeyId,
                                userId: req.body.userId,
                                dateTime: req.body.dateTime,
                                amount: earned,
                                commission: commission1
                            }, function (err, earningInfo) {
                                if (err) {
                                    next(err);
                                } else {
                                    res.json({
                                        status: true,
                                        message: "Payment has been made successfully",
                                        result: result
                                    })
                                }
                            })
                        }
                    }
                )
        })
    },

    viewearning: function (req, res, next) {
        var totalAmount = 0;
        let earningsArray = [];
     
        journeys.find({
            driverId: req.params.userId,
            driverResponse: {
                "$in": ["Paid", "Completed"]
            }

        }, function (err, results) {
            if (results.length <= 0) {
                return res.status(200).send({
                    status: true,
                    message: 'No Earning Details'
                });
            } else {
             var count=0;
                for (let product of results) {
                    earningModel.findOne({
                            journeyId: product._id
                        }, function (err, earningInfo) {
                            if (err) {
                                next(err);
                            } else {
                                totalAmount += parseFloat(earningInfo.amount);
                                earningsArray.push({
                                    driverResponse: product.driverResponse,
                                    dateTime: earningInfo.dateTime,
                                    amount: earningInfo.amount,
                                })
                                count++;
                                if (count == results.length) {
                                    totalAmount=totalAmount.toFixed(2);
                                    return res.status(200).send({
                                        status: true,
                                        message: 'Earning Details have been retrieved',
                                        totalAmount,
                                        earningsArray
                                    });
                                }
                            }
                        }
                    )
                }
            }
        })
    },

    viewcommission:function(req,res,next) {

        earningModel.find()
        .then(earningDetails => {
            res.json({
                status: true,
                message: "All the earning details are retrieved",
                product: earningDetails

            });
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving earning details."
            });
        });
    }
};